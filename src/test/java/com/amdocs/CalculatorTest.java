package com.amdocs;
import org.junit.Assert;
import org.junit.Test;
import com.amdocs.Calculator;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
public class CalculatorTest {
    @Test
    public void testAdd() {
        final Calculator calc = mock(Calculator.class);
        when(calc.add()).thenReturn(9);
        final int getAdd = new Calculator().add();
        Assert.assertEquals(9, getAdd);
    }

    @Test
    public void testSub() {
        final Calculator calc = mock(Calculator.class);
        when(calc.sub()).thenReturn(3);
        final int getAdd = new Calculator().sub();
        Assert.assertEquals(3, getAdd);
    }
}
