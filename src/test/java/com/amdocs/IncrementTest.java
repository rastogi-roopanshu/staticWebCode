package com.amdocs;
import org.junit.Assert;
import org.junit.Test;
import  com.amdocs.Increment;
public class IncrementTest {
    @Test
    public void testDecreaseCounter(){
        final int counter = new Increment().decreasecounter(2);
        Assert.assertEquals(1, counter);
	final int counter1 = new Increment().decreasecounter(0);
 	Assert.assertEquals(1, counter1);
	final int counter2 = new Increment().decreasecounter(1);
	Assert.assertEquals(1, counter2);
    }
}
